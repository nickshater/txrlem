package types

// Die is the typing for the outcome of a Roll()
type Die struct {
	Value int
	Hold  bool
}

// Hand holds the players current hand
type Hand struct {
	Dice [3]Die
}

// Roll is the type for a community die
type Roll struct {
	Value int
}

// Flop is the first phase the community board
type Flop struct {
	Dice [3]Die
}

// Turn is the second phase of the community board
type Turn struct {
	Dice [1]Die
}

// River is the third and final phase of the community board
type River struct {
	Dice [1]Die
}

// Board holds the community rolls
type Board struct {
	F Flop
	T Turn
	R River
}

// ScoreCard holds the scoring system
type ScoreCard struct {
	Royal     int
	FullHouse int
	LgStrt    int
	SmStrt    int
	FourKind  int
	ThreeKind int
	Sixes     int
	Fives     int
	Fours     int
	Threes    int
	Twos      int
	Ones      int
}

// Singles is the scoring type for single type rolls
type Singles struct {
	Ones   int
	Twos   int
	Threes int
	Fours  int
	Fives  int
	Sixes  int
}

// Royal is the scoring type for royal rolls
type Royal struct {
	Royal int
}

// Chance is the scoring type for chance rolls
type Chance struct {
	Chance int
}

// ThreeOfAKind is the scoring type for rolls with 3 like rolls
type ThreeOfAKind struct {
	ThreeOfAKind int
}

// FourOfAKind is the scoring type for rolls with 4 like rolls
type FourOfAKind struct {
	FourOfAKind int
}

// FullHouse is the scoring type for rolls with 3 of a kind and a pair together
type FullHouse struct {
	FullHouse int
}
