package score

import (
	"sort"

	"github.com/nickshater/txrlem/types"
)

// SortBoard sorts the board for help with scoring
func SortBoard(board types.Board) []int {
	sorted := make([]int, 5)
	sorted[0] = board.F.Dice[0].Value
	sorted[1] = board.F.Dice[1].Value
	sorted[2] = board.F.Dice[2].Value
	sorted[3] = board.T.Dice[0].Value
	sorted[4] = board.R.Dice[0].Value

	sort.Ints(sorted)
	return sorted
}

// SortHand sorts the players hand for help with scoring
func SortHand(hand types.Hand) []int {
	sorted := make([]int, 3)
	sorted[0] = hand.Dice[0].Value
	sorted[1] = hand.Dice[1].Value
	sorted[2] = hand.Dice[2].Value

	sort.Ints(sorted)
	return sorted
}

// HandCount returns the number of instances of test int in hand
func HandCount(h []int, test int) int {
	count := 0
	var total int
	for _, a := range h {
		if a == test {
			count++
		}
	}
	if count <= 2 {
		total = count * test
	} else if count > 2 {
		total = test * 2
	}
	return total
}

// BoardCount returns the number of instances of test int on board
func BoardCount(h []int, test int) int {
	count := 0
	var total int
	for _, a := range h {
		if a == test {
			count++
		}
	}
	if count <= 3 {
		total = count * test
	} else if count > 3 {
		total = test * 3
	}
	return total
}

// CheckSingles checks for scores for singles categories
func CheckSingles(board []int, hand []int) types.Singles {
	var score types.Singles
	scores := make([]int, 6)
	for i := 1; i <= 6; i++ {
		b := BoardCount(board, i)
		h := HandCount(hand, i)
		total := (b + h)
		scores[i-1] = total
	}
	score.Ones = scores[0]
	score.Twos = scores[1]
	score.Threes = scores[2]
	score.Fours = scores[3]
	score.Fives = scores[4]
	score.Sixes = scores[5]

	return score
}

// CheckRoyal looks for 5 of a kind in a roll
func CheckRoyal(board []int, hand []int) types.Royal {
	var score types.Royal
	score.Royal = 0
	for i := 1; i <= 6; i++ {
		b := BoardCount(board, i)
		h := HandCount(hand, i)
		if b == (i*3) && h == (i*2) {
			score.Royal = 50
		}
	}
	return score
}

// Check3OfAKind checks for 3 of a kind score
func Check3OfAKind(board []int, hand []int) types.ThreeOfAKind {

	var score types.ThreeOfAKind
	score.ThreeOfAKind = 0
	for i := 1; i <= 6; i++ {
		// Check for instances of roll
		b := BoardCount(board, i) / i
		h := HandCount(hand, i) / i
		t := b + h
		// Progress if 3 of a kind exists
		if b >= 3 || ((h >= 1) && (b >= 1) && ((b + h) >= 3)) {

			// Create copies of hand and board to find next highest die value
			mzb := make([]int, len(board))
			copy(mzb, board)
			mzh := make([]int, len(hand))
			copy(mzh, hand)

			// Add 3 * roll value
			total := i * 3

			// Set 4 of a kind rolls to zero to find next highest value
			zb, zh := SetToZero(mzb, mzh, i, t, 3)

			// Check for whether to use the board or hand values for 4th and 5th die
			switch {
			case h == 0:
				total += zh[2]
				total += zh[1]
			case b == 1:
				total += zb[4]
				total += zb[3]
			default:
				if zb[4] >= zh[2] {
					total += zb[4]
					zb[4] = 0
					sort.Ints(zb)
				} else {
					total += zh[2]
					zh[2] = 0
					sort.Ints(zh)
				}

				if zb[4] >= zh[2] {
					total += zb[4]
				} else {
					total += zh[2]
				}
			}

			if total > score.ThreeOfAKind {
				score.ThreeOfAKind = total
			}
		}
	}
	return score
}

// Check4OfAKind checks for 4 of a kind score
func Check4OfAKind(board []int, hand []int) types.FourOfAKind {
	var score types.FourOfAKind
	score.FourOfAKind = 0
	for i := 1; i <= 6; i++ {
		// Check for instances of roll
		b := BoardCount(board, i) / i
		h := HandCount(hand, i) / i
		t := b + h

		// Progress if 4 of a kind exists
		if (h >= 1) && (b >= 2) && ((b + h) >= 4) {

			// Create copies of hand and board to find next highest die value
			mzb := make([]int, len(board))
			copy(mzb, board)
			mzh := make([]int, len(hand))
			copy(mzh, hand)

			// Add 4 * roll value
			total := i * 4

			// Set 4 of a kind rolls to zero to find next highest value
			zb, zh := SetToZero(mzb, mzh, i, t, 4)

			// Check for whether to use the board or hand values for 5th die
			switch {
			case h == 1:
				total += zh[2]
			case b == 2:
				total += zb[4]
			default:
				if zb[4] >= zh[2] {
					total += zb[4]
				} else {
					total += zh[2]
				}
			}

			if total > score.FourOfAKind {
				score.FourOfAKind = total
			}
		}
	}
	return score
}

// CheckChance returns the score for a chance roll
func CheckChance(board []int, hand []int) types.Chance {
	var score types.Chance
	b := board[2:]
	h := hand[1:]
	bt := 0
	ht := 0
	for _, bd := range b {
		bt += bd
	}
	for _, hd := range h {
		ht += hd
	}
	score.Chance = bt + ht
	return score
}

// CheckFullHouse checks for full house roll
// REFACTOR LATER
func CheckFullHouse(board []int, hand []int) types.FullHouse {
	var score types.FullHouse

	for i := 1; i <= 6; i++ {
		it, jt := 0, 0
		ib := BoardCount(board, i) / i
		ih := BoardCount(hand, i) / i
		it = (ib + ih)

		if it >= 2 {

			for j := 1; j <= 6; j++ {
				if j != i {
					jb := BoardCount(board, j) / j
					jh := BoardCount(hand, j) / j
					jt = (jb + jh)

					// Check Scoring Conditions
					if it >= 2 && jt >= 3 {
						switch {
						case ih >= 2 && jb >= 3:
							score.FullHouse = 25
						case ih >= 1 && jh >= 1 && ib >= 1 && jb >= 2:
							score.FullHouse = 25
						case ib >= 2 && jb >= 1 && jh >= 2:
							score.FullHouse = 25
						default:
							break
						}
					}

					if it >= 3 && jt >= 2 {
						// Check Scoring Conditions
						switch {
						case jh >= 2 && ib >= 3:
							score.FullHouse = 25
						case jh >= 1 && ih >= 1 && ib >= 2 && jb >= 1:
							score.FullHouse = 25
						case jb >= 2 && ib >= 1 && ih >= 2:
							score.FullHouse = 25
						default:
							break
						}
					}
				}
			}
		}
		if score.FullHouse == 25 {
			break
		}
	}
	return score
}

// SetToZero replaces all instances of an int with 0 in a slice
func SetToZero(board []int, hand []int, idx int, t int, k int) ([]int, []int) {
	bc := t - k
	hc := t - k

	for i, b := range board {
		if b == idx {
			if bc < 3 {
				board[i] = 0
			}
			bc++
		}
	}

	for j, h := range hand {
		if h == idx {
			if hc < 2 {
				hand[j] = 0
			}
			hc++
		}
	}

	sort.Ints(board)
	sort.Ints(hand)
	return board, hand
}
