package dice

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/nickshater/txrlem/score"
	"github.com/nickshater/txrlem/types"
)

// Roll simulates the roll of a die
func Roll() types.Die {
	rng := rand.NewSource(time.Now().UnixNano())
	var roll types.Die
	roll.Value = rand.New(rng).Intn(6) + 1
	roll.Hold = false
	return roll
}

// Deal rolls 3 dice at once
func Deal() types.Hand {
	var hand types.Hand
	hand.Dice[0] = Roll()
	hand.Dice[1] = Roll()
	hand.Dice[2] = Roll()

	fmt.Println("you rolled: ", hand.Dice[0].Value, hand.Dice[1].Value, hand.Dice[2].Value)
	return hand
}

// Flop deals 3 dice to the board
func Flop() types.Flop {
	var flop types.Flop
	flop.Dice[0] = Roll()
	flop.Dice[1] = Roll()
	flop.Dice[2] = Roll()

	return flop
}

// Turn deals 2 dice to the board
func Turn() types.Turn {
	var turn types.Turn
	turn.Dice[0] = Roll()

	return turn
}

// River deals the last die to the board
func River() types.River {
	var river types.River
	river.Dice[0] = Roll()

	return river
}

// StayHold lets the player keep or reroll a die
func StayHold(h types.Hand) types.Hand {
	var resp string
	for i := range h.Dice {
		fmt.Println("die ", (i + 1), " is: ", h.Dice[i].Value)
		fmt.Println("do you want to 'keep' or 'roll' it")
		fmt.Scanf("%s", &resp)
		if resp == "keep" {
			h.Dice[i].Hold = true
		} else {
			h.Dice[i].Hold = false
		}
	}
	return h
}

// ReRoll rolls again for the non held die
func ReRoll(h types.Hand) types.Hand {

	for i := range h.Dice {
		if h.Dice[i].Hold == false {
			h.Dice[i] = Roll()
		}
	}

	return h
}

// GameRound covers the actions of a turn
func GameRound() {
	var b types.Board
	hand := Deal()
	b.F = Flop()
	fmt.Println("the board is: ", b.F.Dice[0].Value, " ", b.F.Dice[1].Value, " ", b.F.Dice[2].Value)
	hand = StayHold(hand)
	hand = ReRoll(hand)
	fmt.Println("your hand is: ", hand.Dice[0].Value, " ", hand.Dice[1].Value, " ", hand.Dice[2].Value)
	b.T = Turn()
	fmt.Println("the board is: ", b.F.Dice[0].Value, " ", b.F.Dice[1].Value, " ", b.F.Dice[2].Value, " ", b.T.Dice[0].Value)
	hand = StayHold(hand)
	hand = ReRoll(hand)
	fmt.Println("your hand is: ", hand.Dice[0].Value, " ", hand.Dice[1].Value, " ", hand.Dice[2].Value)
	b.R = River()
	fmt.Println("the board is: ", b.F.Dice[0].Value, " ", b.F.Dice[1].Value, " ", b.F.Dice[2].Value, " ", b.T.Dice[0].Value, " ", b.R.Dice[0].Value)
	score.SortBoard(b)
}

// TestFunc tests the scoring system
func TestFunc() {
	hand := Deal()
	var board types.Board
	board.F = Flop()
	board.T = Turn()
	board.R = River()
	sortedboard := score.SortBoard(board)
	sortedhand := score.SortHand(hand)
	scores := score.CheckSingles(sortedboard, sortedhand)
	royal := score.CheckRoyal(sortedboard, sortedhand)
	tKind := score.Check3OfAKind(sortedboard, sortedhand)
	fKind := score.Check4OfAKind(sortedboard, sortedhand)
	fHouse := score.CheckFullHouse(sortedboard, sortedhand)
	chance := score.CheckChance(sortedboard, sortedhand)

	fmt.Println(sortedboard, sortedhand)

	fmt.Println("Ones Score: ", scores.Ones)
	fmt.Println("Twos Score: ", scores.Twos)
	fmt.Println("Threes Score: ", scores.Threes)
	fmt.Println("Fours Score: ", scores.Fours)
	fmt.Println("Fives Score: ", scores.Fives)
	fmt.Println("Sixes Score: ", scores.Sixes)
	fmt.Println("3 of a kind score ", tKind.ThreeOfAKind)
	fmt.Println("4 of a kind score ", fKind.FourOfAKind)
	fmt.Println("Full House Score ", fHouse.FullHouse)
	fmt.Println("Chance Score", chance.Chance)
	fmt.Println("Royal Score: ", royal.Royal)

}
